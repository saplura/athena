#include "InDetTrigPrepRawDataFormat/SCT_TrgClusterization.h"
#include "InDetTrigPrepRawDataFormat/Pixel_TrgClusterization.h"
#include "InDetTrigPrepRawDataFormat/TRT_TrgRIO_Maker.h"
#include "InDetTrigPrepRawDataFormat/FTK_TrackMaker.h"


using namespace InDet;

DECLARE_COMPONENT( InDet::SCT_TrgClusterization )
DECLARE_COMPONENT( InDet::Pixel_TrgClusterization )
DECLARE_COMPONENT( InDet::TRT_TrgRIO_Maker )
DECLARE_COMPONENT( InDet::FTK_TrackMaker )

